﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;
using System.IO;


//this line is for cherry pick
//need to add cropping and compression to image files
[assembly: log4net.Config.XmlConfigurator(Watch = true)]
namespace ELImageProcessor
{
    class Program
    {
        public static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        static void Main(string[] args)
        {

            map[] mapping;
            using (StreamReader file = File.OpenText(@"data.json"))
            {
                JsonSerializer serializer = new JsonSerializer();
                mapping = (map[])serializer.Deserialize(file, typeof(map[]));
            }

            using (CAGEntities db = new CAGEntities())
            {
                foreach (var m in mapping)
                {
                    bool hasSwatchImage = true, hasProductImage = true;
                    string newSwatchFolder = @"New\Swatch";
                    string newProductFolder = @"New\Products";
                    if (!Directory.Exists(newSwatchFolder)) Directory.CreateDirectory(newSwatchFolder);
                    if (!Directory.Exists(newProductFolder)) Directory.CreateDirectory(newProductFolder);

                    string swatchPath = @"Estee Lauder\color_chip\200x200\" + m.filename + ".jpg";
                    string productPath = @"Estee Lauder\product_image\1500x1500-ISC\" + m.filename + ".jpg";

                    if (!File.Exists(swatchPath))
                    {
                        log.Info("No swatch for " + m.SKU);
                        hasSwatchImage = false;
                    }

                    if (!File.Exists(productPath))
                    {
                        log.Info("No swatch for " + m.SKU);
                        hasProductImage = false;
                    }

                    if (hasSwatchImage || hasProductImage)
                    {
                        string[] shade = { "Shade", "Color" };
                        long[] shadeID = { 98, 102, 14, 30, 104, 107 };
                        var product = db.Product.Where(x => x.SKU == m.SKU).FirstOrDefault();

                        if (product == null)
                        {
                            log.Info("No product for " + m.SKU);
                            continue;
                        }

                        var attrmap = db.ProductAttributeValueMap.Where(x => x.Product_AutoID == product.AutoID && shadeID.Contains(x.ProductAttribute_AutoID)).ToList();

                        if (attrmap.Count == 0)
                        {
                            log.Info("No shade/color attribute for " + m.SKU);
                            continue;
                        }

                        string attrValue = "";
                        foreach (var attr in attrmap)
                        {
                            if (string.IsNullOrEmpty(attrValue))
                            {
                                var av = db.AttributeValue.Where(x => x.AutoID == attr.ProductAttributeVAlue_AutoID && x.ProductAttribute_Value.Contains("/")).FirstOrDefault();
                                if (av != null) attrValue = av.ProductAttribute_Value.Split('/')[1].Trim();
                            }
                            else
                            {
                                log.Info("ANOTHER attribute found  for SKU: " + m.SKU);
                            }
                        }
                        

                        if (string.IsNullOrEmpty(attrValue))
                        {
                            log.Info("No Attribute for " + m.SKU);
                        }
                        else
                        {
                            string filename = string.Format("SKU{0}_{1}.jpg", m.SKU, attrValue);

                            if (hasSwatchImage)
                            {
                                File.Copy(swatchPath, Path.Combine(newSwatchFolder, filename));
                            }

                            if (hasProductImage)
                            {
                                File.Copy(productPath, Path.Combine(newProductFolder, filename));
                            }
                        }

                    }

                }
            }
        }
    }
    
    public class map
    {
        [JsonProperty("SKU_CODE")]
        public string SKU { get; set; }
        [JsonProperty("Ref")]
        public string filename { get; set; }
    }
	
	public class crop
	{
		
	}
	
	public class compress
	{
		
	}
}
